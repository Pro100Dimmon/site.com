<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ArticlesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $i=1;
        while ($i<6){ DB::table('posts')->insert([
                'title'       => 'Продам iPhone' .$i,
                'description' => 'Продам iphone  gb.Состояние отличное.В комплекте наушники,зарядка',
                'author_name' => 'Dmitriy Svinar',
                'slug' => 'author',
                'active' => '',
            ]);
        $i++;
    }
}

}